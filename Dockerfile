FROM python:3.8-slim
# Зависимости меняются реже всего
COPY requirements/production.txt /tmp/requirements/production.txt
RUN pip install --no-cache -r  /tmp/requirements/production.txt

# Код запуска меняется реже чем код приложения
WORKDIR /usr/src/app
COPY run_service.py run_servicer.py

# Код приложения меняется чаще всего
COPY dist /tmp/dist
RUN pip install /tmp/dist/*

ENTRYPOINT ["python", "run_servicer.py", "--port", "8085"]
EXPOSE 8085
# Порт можно добавить как переменную и передовать при запуске, но это не имеет особого смысла в данной ситуации.
# Для сборки зависимостей можно использовать multi-stage, но это так же не имеет особого смысла и лишь сделает все сложнее.
